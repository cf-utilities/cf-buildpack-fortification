setup_cf_app() {
  local this_app_prefix=$1; shift
  local manifest=$1; shift
  local repo=$1; shift
  local app_dir=$(mktemp -d -t XXXXXXXX)
  local app_name=${CF_APP_PREFIX:-test}-${this_app_prefix}-$(
    dd if=/dev/urandom bs=1 count=1024 2>/dev/null \
    | { md5sum 2>/dev/null || md5 2>/dev/null; } \
    | cut -c1-16)

  git clone $repo $app_dir >/dev/null
   
  for FILE in "$@"; do
    local src=$(echo "${FILE}::" | cut -d: -f1)
    local tgt=$(echo "${FILE}::" | cut -d: -f2)
    tgt=${app_dir}/${tgt}
    cp ${src} ${tgt} >/dev/null
  done

  cf target >&2
  cf push ${app_name} -f ${manifest} -p ${app_dir} >&2

  local app_route=$(cf app ${app_name} | awk '$1~/^urls:/{print $2}')

  echo "export APP_NAME=${app_name} APP_ROUTE=${app_route}; cleanup_cf_app() { rm -rf ${app_dir}; cf delete -f -r ${app_name}; }"
}
