export BATS_GLOBAL_SETUP_FILE="${BATS_TMPDIR}/bats.import.$PPID"

bats_run_global_setup_once() {
  [ $BATS_TEST_NUMBER -eq 1 ]
}

bats_import_setup() {
  . ${BATS_GLOBAL_SETUP_FILE}
}

bats_write_setup() {
  cat >${BATS_GLOBAL_SETUP_FILE}
}

bats_run_global_teardown_once() {
  if [ "$BATS_TEST_NAME" = "${BATS_TEST_NAMES[((${#BATS_TEST_NAMES[*]}-1))]}" ]; then
    rm -f ${BATS_GLOBAL_SETUP_FILE}
    return 0
  else
    return 1
  fi
}

runFuncIfDefined() {
  local func=$1; shift
  ! declare -f $func >/dev/null || $func "$@"
}

setup() {
  if bats_run_global_setup_once; then
    { runFuncIfDefined beforeAllDoAndPrintEvalableString || exit 1; } \
    | bats_write_setup
  fi
  bats_import_setup
  runFuncIfDefined beforeEach
}

teardown() {
  runFuncIfDefined afterEach
  if bats_run_global_teardown_once; then
    runFuncIfDefined afterAll
  fi
}
