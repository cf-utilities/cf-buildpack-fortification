assertSuccessfulWithinTimeout() {
  local command=${1}
  local timeout=${2:-10}
  local interval=${3:-2}

  if [ -z "$command" ]
  then
    echo "USAGE: assertSuccessfulWithinTimeout COMMAND [TIMEOUT] [INTERVAL]"
    return -1
  fi

  local waited=0
  until eval $(command); do
    sleep $interval
    let waited+=interval

    if [ $waited -gt $timeout ]; then
      echo Timed out after $waited seconds
      return -1
    fi
  done

  return 0
}