#!/usr/bin/env bats

@test "the same jq binary is available as in the buildpack stager" {
  run jq --version
  [ "${status}" = "0" ]
  echo "${output}" | grep -q '1.3'
}

@test "the cf binary is available" {
  run cf --version
  [ "${status}" = "0" ]
}