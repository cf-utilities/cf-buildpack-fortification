#!/usr/bin/env bats

@test "buildpack standard scripts are executable" {
  [ -x bin/detect ]
  [ -x bin/compile ]
  [ -x bin/release ]
}
