#!/usr/bin/env bats

# set -x

@test "cf push :: cf push fails on app not binding to internal port" {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  appdir=$(mktemp -d -t XXXXXXXX) || true
  appname=${CF_APP_PREFIX:-testing}-${RANDOM}-cf-push-fail-no-bind
  cp tmp/f11n-buildpack-WIP.tgz ${appdir}/
  cp tests/fixtures/buildpacks/only-buildpack-wip ${appdir}/.buildpacks
  run cf push ${appname} -t 15 -f tests/fixtures/manifests/multi-sleep-inf.yml -p ${appdir}
  [ "${status}" -ne 0 ]
  ! echo "${output}" | grep -q "App started"
  run cf delete -f -r ${appname}
  rm -rf ${appdir}/
}