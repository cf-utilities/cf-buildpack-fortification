#!/usr/bin/env bats

@test "origin protection :: disabled :: 200 response with no creds" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "200" ]
}

@test "origin protection :: disabled :: 200 response with wrong creds" {
  run curl -so/dev/null --user wrong:password --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "200" ]
}

@test "origin protection :: disabled :: 200 response with correct creds" {
  run curl -so/dev/null --user user1:password1 --write-out "%{http_code}" ${APP_ROUTE}/
  [ "${output}" = "200" ]
}

@test "origin protection :: disabled :: HelloWorld response body with correct creds" {
  run curl -s --user user1:password1 ${APP_ROUTE}/
  echo "${output}" | grep -q "Hello, World!"
}

@test "origin protection :: disabled :: HelloWorld response body with wrong creds" {
  run curl -s --user wrong:password ${APP_ROUTE}/
  echo "${output}" | grep -q "Hello, World!"
}

load helpers/cf-app-tools
load helpers/bats-tools

beforeAllDoAndPrintEvalableString() {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  setup_cf_app \
    "origin-protection-disabled" \
    tests/fixtures/manifests/origin-protection-disabled.yml \
    https://github.com/jabley/hello-python-web \
    tests/fixtures/buildpacks/basic-auth.python:.buildpacks \
    tmp/f11n-buildpack-WIP.tgz
}

afterAll() {
  cleanup_cf_app
}
