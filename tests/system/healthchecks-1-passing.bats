#!/usr/bin/env bats

@test "healthchecks :: 1 configured, passing :: /not-empty-file.txt: 200 response" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/not-empty-file.txt
  [ "${output}" = "200" ]
}
@test "healthchecks :: 1 configured, passing :: /_proxy/client-check: 200 response" {
  run curl -so/dev/null --write-out "%{http_code}" ${APP_ROUTE}/_proxy/client-check
  [ "${output}" = "200" ]
}

load helpers/cf-app-tools
load helpers/bats-tools

beforeAllDoAndPrintEvalableString() {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  setup_cf_app \
    "healthchecks-1" \
    tests/fixtures/manifests/healthchecks-1-passing.yml \
    https://github.com/jpluscplusm/staticfile-empty-repo \
    tests/fixtures/buildpacks/healthchecks.staticfile:.buildpacks \
    tests/fixtures/not-empty-file.txt:webroot/ \
    tmp/f11n-buildpack-WIP.tgz
}

afterAll() {
  cleanup_cf_app
}
