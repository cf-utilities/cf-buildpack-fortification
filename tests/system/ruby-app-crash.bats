#!/usr/bin/env bats

@test "ruby hello world :: /crash :: returns 502 and the crash appears in the event log" {
  assertSuccessfulWithinTimeout "curl --fail -so/dev/null --user user1:password1 ${APP_ROUTE} >/dev/null 2>/dev/null" 10 1
  run curl -so/dev/null --user user1:password1 -X POST -d foo=bar --write-out "%{http_code}" ${APP_ROUTE}/crash
  [ "${output}" = "502" ]
  run cf events ${APP_NAME}
  echo ${output} | grep -q app\.crash
}

@test "ruby hello world :: /crash :: 1 crash visible in the event log" {
  assertSuccessfulWithinTimeout '[ $(cf events ${APP_NAME} | grep -c app\.crash) -eq 1 ]' 20 2
}

load helpers/cf-app-tools
load helpers/bats-tools
load helpers/assertions

beforeAllDoAndPrintEvalableString() {
  [ -s tmp/f11n-buildpack-WIP.tgz ]
  setup_cf_app \
    "ruby-hw" \
    tests/fixtures/manifests/ruby-hw.yml \
    https://github.com/DigitalInnovation/ruby-hello-world \
    tests/fixtures/buildpacks/basic-auth.ruby:.buildpacks \
    tmp/f11n-buildpack-WIP.tgz
}

afterAll() {
  cleanup_cf_app
}
