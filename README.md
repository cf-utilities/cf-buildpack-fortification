# The Application Fortification ("F11N") Buildpack

## Summary

This buildpack installs various metric reporting services into an application's
runtime.  It relies on a secondary buildpack, the Process Supervisor
(https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor) to
instantiate and regulate these services.

It also places a robust HTTP reverse proxy between the Cloud Foundry (CF)
router and the application. The proxy protects the application from overload by
limiting the number of concurrent requests which the application instance
receives, and provides extremely detailed logging and metrics for requests at
the HTTP protocol layer. This buildpack is not currently usable by TCP-routed
CF applications.

The proxy can also, once configured to do so (see "Configuration",
below), provide a degree of Origin Protection by only allowing authenticated
consumers to communicate with the application. This allows well-behaved
12-factor applications to be distributed across a set of CF installations,
having their requests load-balanced by a CDN which injects an authorisation
key. Thus the only route to the application is via the CDN, ensuring all
consumer requests satisify the CDN's application protection mechanism; there is
no direct "back-door" into any individual CF installation.

Finally, the buildpack normalises a set of diverse public Cloud Foundry
providers' runtime differences so that applications do not need to be aware
which CF it has been deployed on to. These differences include things such as
request protocol (TLS/HTTPS/HTTP), unique request IDs, and how the CF's
name/region is communicated to the application for logging, etc.

## Usage

From an application's `manifest.yml` or `cf push -b <buildpack>` invocation,
extract the current buildpack URL. Construct a `.buildpacks` file in the
application's repository's root directory which looks like this:

    <current buildpack URL>
    https://bitbucket.com/cf-utilities/cf-buildpack-fortification
    https://bitbucket.com/cf-utilities/cf-buildpack-process-supervisor

Next, modify the manifest or `cf push` invocation to reference the Multi
buildpack:

    https://bitbucket.com/cf-utilities/cf-buildpack-multi

NB At present the Multi buildpack only works with external buildpacks, not
those built in to the target Cloud Foundry. This limitation may be addressed in
the future.

Finally, `cf push` the application. During the staging process, you should
notice lines roughly matching these scroll by, after the application's usual
buildpack/runtime has finished:

    =====> Downloading Buildpack: https://bitbucket.org/cf-utilities/cf-buildpack-fortification
    =====> Detected Framework: Fortification
    =====> Downloading Buildpack: https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor
    =====> Detected Framework: Process Supervisor
    Using release configuration from last framework (Process Supervisor).

## Configuration

The following environment variables can be set by an application's
`manifest.yml` or `cf set-env` invocations in order to change the behaviour of
the buildpack during staging and its runtime.

Variables which represent times are specified in the format defined here,
including their unit suffix:
https://cbonte.github.io/haproxy-dconv/1.6/configuration.html#2.4

`FBP_RP_NAMESPACE_PREFIX`: a string containing the URL path prefix which the
proxy dedicates for its own use on inbound requests. Requests for URLs starting
with this string will never be received by the application.  **Default**:
`/_proxy`.

`FBP_RP_GLOBAL_MAXCONN`: a positive integer containing the FIXME read the HAP
manual **Default**: 5000.

`FBP_RP_INSTANCE_MAXCONN`: a positive integer containing the maximum number of
concurrent connections which will be accepted by the proxy before FIXME
read the goddam HAProxy manual. **Default**: 1000.

`FBP_RP_APP_MAXCONN`: a positive integer containing the maximum number of
concurrent connections the proxy will allow the app to receive, regardless of
the incoming request rate. NB The proxy does an exceptionally good job of
keeping the pipe beteen itself and application full; in other words, if there
is a significant number of requests to serve, then the application will see
this number of concurrent connections at all times. This *doesn't* represent "N
per second" or anything similar - this is the maximum simultaneous requests in
flight to the backend application. It is strongly recommended you tune this
value before production deployment. The default is deliberately set too high so
you can observe the requests-in-flight inflection point at which a single
instance degrades under load, and can then specify an application-appropriate
value. See "Logging", below, for how to observe the maximum number of
requests-in-flight. **Default**: 1000.

`FBP_RP_DEFAULT_TIMEOUT_CONNECT`: a time variable detailing the maximum time
that the proxy will wait to connect the application before serving an HTTP 503.
It's recommended you leave this at its default. **Default**: 3s.

`FBP_RP_DEFAULT_TIMEOUT_CLIENT_INACTIVITY`: a time variable representing the
maximum time the proxy will wait for the client to send or acknowledge data
when it it's supposed to do so. **Default**: 30s.

`FBP_RP_DEFAULT_TIMEOUT_CLIENT_REQUEST`: a time variable representing the
maximum time the proxy will wait for a complete request header from the client.
This default is set quite low as the *proxy's* client is the Cloud Foundry
router, not the external user-agent. **Default**: 3s.

`FBP_RP_DEFAULT_TIMEOUT_SERVER_INACTIVITY`: a time variable representing the
maximum time the proxy will wait for an application to begin responding to a
request. **Default**: 65s

`FBP_RP_DEFAULT_TIMEOUT_QUEUE`: a time variable representing the maximum time
the proxy will wait for a free connection slot to the application to become
available. **Default**: 30s

`FBP_RP_DISABLE_ORIGIN_PROTECTION`: a boolean indicating if user-based access
controls to the application should be disabled or not. **Default**: "false".
See also: `FBP_RP_ACTIVE_USERNAMES`, `FBP_RP_USER_*`.

`FBP_RP_ACTIVE_USERNAMES`: a comma-separated list of usernames which are
permitted to access the application.  **Default**: "". See also:
`FBP_RP_USER_*`.

`FBP_RP_USER_*`: the variable hierarchy prefixed with `FBP_RP_USER_` represents
username/password combinations which can be referenced in the
`FBP_RP_ACTIVE_USERNAMES` list. The usernames are case sensitive, as in the
following example:

```
FBP_RP_USER_jamie: unencrypted-password-1
FBP_RP_USER_sam: unencrypted-password-2
```

**NB** Adding a user in the `FBP_RP_USER_` hierarchy does **not** automatically
add the user to the list of usernames allowed to access the application.

### Healthchecks

`FBP_RP_HC_*`: the variable hierarchy prefixed with `FBP_RP_HC_` sets up
healthchecks which the request proxy will perform on the application
periodically, and report asynchronously to anyone requesting specific HTTP URIs
from the instance. The key here is the asynchronicity of the calls: a backend
check to the application can be performed relatively infrequently if, for
instance, it consumes a non-trivial amount of application resource to do so;
the success or failure of this call (scoped to just this application instance)
can then be checked aggressively by monitoring services without transmitting
that load to the application or its dependencies.

A healthcheck variable has the following structure:

`"<client-side path>,<application-side path>,<status code success regex>,<check frequency when healthy>,<check frequency when unhealthy>"`

Here is a valid example:

`"/check-1,/health/long-check,^2,60s,5s"`

The component parts are as follows:

* Client-side path: this is a URL path *suffix* which, when added to the value set
via `FBP_RP_NAMESPACE_PREFIX`, is accessible from outside the Cloud Foundry
platform. In the above example, with the default proxy namespace prefix, this
would result in the path `/_proxy/check-1` representing the state of this
healthcheck. NB This cannot contain a query-string.

* Application-side path: this is a URL path which is fetched from the application
periodically. It is *not* a suffix, as with the client-side setting, and *can*
contain a query-string. NB No `Host` header is passed through with this check.

* Status code success regex: this is a regular expression against which the
application's HTTP response code is compared. A regular expression match
indicates a healthy application. For example, `^[23]` would allow 2xx and 3xx
responses, but would cause the healthcheck to fail if the application responded
with 4xx or 5xx responses.

* Check frequency when healthy: this is a time variable indicating the frequency
with which the application will be polled at a time when previous responses
indicated that the application was healthy.

* Check frequency when unhealthy: this is a time variable indicating the
frequency with which the application will be polled at a time when previous
responses indicated that the application was **not** healthy.

Several healthchecks may be configured simultaneously, but care must be taken
not to configure overlapping or conflicting client-side paths. Multiple
healthchecks can check the same application path if so configured - the checks
will not be merged and each healthcheck's state will not influence any other
healthcheck. However this setup is not recommended.

Each healthcheck is given a name, indicated by the environment variable name's
suffix.  The environment variable name's prefix is `FBP_RP_HC_`, and the name
is a case-sensitive suffix to this prefix. For example:

`FBP_RP_HC_longcheck: "/check-1,/health/long-check,^2,60s,5s"`

### Disabling F11N's built in stats/logs reporting services

The F11N buildpack installs several lightweight services which periodically report metrics and logs from each application instance. This data flows into the Cloud Foundry loggregator, which then routes the logs as has been configured at the PaaS level. Any of these services may be disabled via an environment variable, requiring only an app restart (not restage) to take effect.

`PS_DISABLE_SERVICE_F11N_LOGS_HAPROXY`: a boolean. **Default**: unset == false

`PS_DISABLE_SERVICE_F11N_STATS_CPU`: a boolean. **Default**: unset == false

`PS_DISABLE_SERVICE_F11N_STATS_HAPROXY`: a boolean. **Default**: unset == false

`PS_DISABLE_SERVICE_F11N_STATS_MEMORY`: a boolean. **Default**: unset == false

`PS_DISABLE_SERVICE_F11N_STATS_NETWORK`: a boolean. **Default**: unset == false

# Information the F11N buildpack exposes to the application

## Envvars

CF_ZONE
CF_APP_NAME
CF_SPACE_NAME

## Request headers

TLS
CLIENT_IP

## To Test

### Pre-requisites

For Mac:

* Install [Xcode](https://developer.apple.com/xcode/)

For Windows:

* Install [MinGW](http://www.mingw.org/wiki/Getting_Started)
* Install [Make](http://gnuwin32.sourceforge.net/packages/make.htm)

Install [JQ](https://stedolan.github.io/jq/download/) at v1.3

Test your working environment has the relevant pre-requisites using:

```
make test-prereqs
```

## Running the tests

There are a variety of tests available, each of which is represented as a `make` target.
The list of make targets can be listed using:

```
make
```
and individual targets can be run using

```
make <target_name>
```

When running system tests locally on **PCF Dev**, it is worth throttling the number of parallel tests to avoid exhausting the limited resources set up by default:

```
MAX_TESTS=2 make test-system
```