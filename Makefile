.PHONY: help test test-local test-prereq test-unit test-integration test-system tarball clean
.DEFAULT_GOAL = help
SHELL = /bin/bash

CF_APP_PREFIX := f11n-test-by-$(USER)
BATS = bats$(if $(CI), --tap)
SYSTEM_TESTS ?= tests/system/*.bats
export CF_APP_PREFIX

CF_API   ?= https://api.local.pcfdev.io/
CF_ORG   ?= pcfdev-org
CF_SPACE ?= pcfdev-space
CF_USERNAME  ?= admin
CF_PASSWORD  ?= admin
CF_LOGIN_FLAGS ?= ''
export CF_PASSWORD

test: test-prereq test-unit test-integration test-system clean ## Run all tests
	@echo "*** ALL TESTS PASSED ***"
test-local: test-prereq test-unit test-integration ## Run all non-network-dependent tests
	@echo "*** ALL LOCAL TESTS PASSED ***"

test-prereq: ## Run all prereq tests
	$(BATS) -v >/dev/null
	$(BATS) tests/prerequisites/*.bats
test-unit: ## Run all unit tests
	$(BATS) tests/unit/*.bats
test-integration: ## Run all integration tests
	$(BATS) tests/integration/*.bats
test-system: TEST_FILES := $(wildcard $(SYSTEM_TESTS))
test-system: tarball ## Run all system tests in parallel
	@$(MAKE) -j $(MAX_TESTS) $(TEST_FILES:bats=forcetest)

%.forcetest:
	$(BATS) $(@:forcetest=bats)

tarball:
	@mkdir -p tmp
	@rm -f tmp/f11n-buildpack-WIP.tgz
	@tar cfz tmp/f11n-buildpack-WIP.tgz --exclude ./tmp --exclude .git/ . 2>/dev/null

clean:
	-rm -f tmp/f11n-buildpack-WIP.tgz
	-cf apps | awk '/^$(CF_APP_PREFIX)/{print $$1}' | xargs -P5 -L1 -n1 cf delete -f -r

cf-login:
	cf login -a $(CF_API) -o $(CF_ORG) -s $(CF_SPACE) -u $(CF_USERNAME) -p $${CF_PASSWORD} $(CF_LOGIN_FLAGS)

help: ## Show this help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
